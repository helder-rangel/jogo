package jogo;

public class JogoDigital extends Jogo{
	
	private int anoCriacao;
	private String produtora;
	
	

	public JogoDigital() {
		// TODO Auto-generated constructor stub
	}
	
	public JogoDigital(int id, String nome, int quantidadeJogos, double precoAluguel, int quantidadePessoas, int anoCriacao, String produtora) {
		super(id, nome, quantidadeJogos, precoAluguel, quantidadePessoas);
		this.anoCriacao = anoCriacao;
		this.produtora = produtora;
	}

	public double calcularTotal(double preco, int quantidadePessoas) {
		if(quantidadePessoas > 0) {
			return preco / quantidadePessoas;	
		}
		return -1;
	}

	public int getAnoCriacao() {
		return anoCriacao;
	}

	public void setAnoCriacao(int anoCriacao) {
		this.anoCriacao = anoCriacao;
	}

	public String getProdutora() {
		return produtora;
	}

	public void setProdutora(String produtora) {
		this.produtora = produtora;
	}

	@Override
	public String toString() {
		return "JogoDigital [anoCriacao=" + anoCriacao + ", produtora=" + produtora + "]";
	}
	

	
}
