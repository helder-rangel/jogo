package jogo;

public abstract class Jogo {
	
	private int id;
	private String nome;
	private int quantidadeJogos;
	private double precoAluguel;
	private int quantidadePessoas;
	
	
	public Jogo() {
		super();
	}
	public Jogo(int id, String nome, int quantidadeJogos, double precoAluguel, int quantidadePessoas) {
		super();
		this.id = id;
		this.nome = nome;
		this.quantidadeJogos = quantidadeJogos;
		this.precoAluguel = precoAluguel;
		this.quantidadePessoas = quantidadePessoas;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidadeJogos() {
		return quantidadeJogos;
	}
	public void setQuantidadeJogos(int quantidadeJogos) {
		this.quantidadeJogos = quantidadeJogos;
	}
	public double getPrecoAluguel() {
		return precoAluguel;
	}
	public void setPrecoAluguel(double precoAluguel) {
		this.precoAluguel = precoAluguel;
	}
	public int getQuantidadePessoas() {
		return quantidadePessoas;
	}
	public void setQuantidadePessoas(int quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}
	@Override
	public String toString() {
		return "Jogo [id=" + id + ", nome=" + nome + ", quantidadeJogos=" + quantidadeJogos + ", precoAluguel="
				+ precoAluguel + ", quantidadePessoas=" + quantidadePessoas + "]";
	}
	
	
	

}
