package jogo;

public class JogoRA extends JogoDigital {
	
	private String dispositivo;
	private String marcador;

	public JogoRA() {
		// TODO Auto-generated constructor stub
	}

	public JogoRA(String dispositivo, String marcador) {
		super();
		this.dispositivo = dispositivo;
		this.marcador = marcador;
	}
	

	public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public String getMarcador() {
		return marcador;
	}

	public void setMarcador(String marcador) {
		this.marcador = marcador;
	}

	@Override
	public String toString() {
		return "JogoRA [dispositivo=" + dispositivo + ", marcador=" + marcador + "]";
	}
	
	

}
