package jogo;

import java.util.ArrayList;

public class JogoTabuleiro {
	
	private String estilo;
	private ArrayList<String> adereco = new ArrayList<String>();
	
	public JogoTabuleiro() {
		// TODO Auto-generated constructor stub
	}
	
	public double calcularTotal(double preco, int quantidadePessoas) {
		if(quantidadePessoas > 0) {
			double desconto = preco  * 0.10 ;
			return 	(preco - desconto) / quantidadePessoas ;
		}
		return -1;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public ArrayList<String> getAdereco() {
		return adereco;
	}

	public void setAdereco(ArrayList<String> adereco) {
		this.adereco = adereco;
	}

	@Override
	public String toString() {
		return "JogoTabuleiro [estilo=" + estilo + ", adereco=" + adereco + "]";
	}
	
	

}
