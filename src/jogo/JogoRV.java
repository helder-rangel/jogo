package jogo;

public class JogoRV extends JogoDigital {

	private String tipoCapacete;
	private String estereoscopia;
	
	
	public JogoRV() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public JogoRV(String tipoCapacete, String estereoscopia) {
		super();
		this.tipoCapacete = tipoCapacete;
		this.estereoscopia = estereoscopia;
	}



	public double calcularTotal(double preco, int quantidadePessoas) {
		if(quantidadePessoas > 0) {
			double desconto = preco  * 0.20 ;
			return 	(preco + desconto) / quantidadePessoas ;
		}
		return -1;
	}



	public String getTipoCapacete() {
		return tipoCapacete;
	}



	public void setTipoCapacete(String tipoCapacete) {
		this.tipoCapacete = tipoCapacete;
	}



	public String getEstereoscopia() {
		return estereoscopia;
	}



	public void setEstereoscopia(String estereoscopia) {
		this.estereoscopia = estereoscopia;
	}



	@Override
	public String toString() {
		return "JogoRV [tipoCapacete=" + tipoCapacete + ", estereoscopia=" + estereoscopia + "]";
	}
	
}
