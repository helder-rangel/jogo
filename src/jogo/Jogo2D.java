package jogo;

public class Jogo2D extends JogoDigital {
	
	private String dispositivo;
	private int tipoControle;

	public Jogo2D() {
		// TODO Auto-generated constructor stub
	}

	public Jogo2D(String dispositivo, int tipoControle) {
		super();
		this.dispositivo = dispositivo;
		this.tipoControle = tipoControle;
	}

	public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public int getTipoControle() {
		return tipoControle;
	}

	public void setTipoControle(int tipoControle) {
		this.tipoControle = tipoControle;
	}

	@Override
	public String toString() {
		return "Jogo2D [dispositivo=" + dispositivo + ", tipoControle=" + tipoControle + "]";
	}
	
	
	

}
